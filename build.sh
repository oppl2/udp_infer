#!/bin/bash

set -e

arch=x64
type=Release
build_arch=x64
platform=default
project_dir=$(pwd)
source_build=0
export_package=0

function do_build() {

    if ! [ -x "$(command -v conan)" ]; then
      echo 'Error: conan is not installed.' >&2
      echo 'Run: pip3 install conan && conan config install https://gitlab.com/oppl2/conan_config.git ' >&2
      exit 1
    fi
    local build_dir=${project_dir}/build/${arch}_${type}
    local install_dir=${project_dir}/install/${arch}_${type}

    echo -e "${BOLDGREEN}Build option:${ENDCOLOR}"
    echo -e "  Arch[${arch}] Type[${type}] \n  Build Dir[${build_dir}] \n  Install Dir[${install_dir}]"

    if [ ${source_build} = 1 ]; then
      conan install . --install-folder=${build_dir} -pr:b=x64 -pr:h=${build_arch} -s build_type=${type} --build
    else
      conan install . --install-folder=${build_dir} -pr:b=x64 -pr:h=${build_arch} -s build_type=${type} --build missing
    fi

    # If you need to pass in more environment variables, use the -e parameter, as follows:
    # docker_env
    # conan install . --install-folder=${build_dir} --profile=${build_arch} -s build_type=${type} -e ${docker_env}

    conan build . --build-folder=${build_dir} --package-folder=${install_dir}

    conan package . --build-folder=${build_dir} --package-folder=${install_dir}

    if [ ${export_package} = 1 ]; then
      conan export-pkg . glplc/dev --profile=${build_arch} -s build_type=${type} --package-folder=./install/${arch}_${type} -f
    fi

    echo -e "\033[32m ========================================================== \033[0m"
    echo -e "\033[32m Build ${build_arch}_${type} complete :) \033[0m"
    echo -e "\033[32m ========================================================== \033[0m"
}

usage() {
  echo "Usage: $0 [-t <x64|aarch64>] [-b <debug|release>]"
  echo ""
  echo "optional arguments :"
  echo "  -t  The target platform to be built";
  echo "      Optional value : ['x64', 'aarch64']";
  echo ""
  echo "  -b  Build type, default is 'release'";
  echo "      Optional value : ['debug', 'release']";
  echo ""
  echo "  -c  Clean up the build and install directories";
  echo ""
  echo "  -s  Build all conan packages from source";
  echo ""
  echo "  -e  Export conan packae to local cache";

  exit 1;
}

while getopts ":t:b:hcse" opt; do
  case $opt in
    t)
      case $OPTARG in
        x64|X64)
          arch=x64
          ;;
        aarch64|AARCH64)
          echo "building aarch64"
          arch=aarch64
          ;;
        *)
          echo "Unknown target $t"
          usage
          ;;
      esac
      ;;
    b)
      case $OPTARG in
        Release|release)
          type=Release
          ;;
        Debug|debug)
          type=Debug
          ;;
        *)
          echo "Unknown target $b"
          usage
          ;;
      esac
      ;;
    h)
      usage
      ;;
    c)
      git clean -fd
      echo -e "\033[32m ========================================================== \033[0m"
      echo -e "\033[32m Cleaned up ./install and ./build Folders. :) \033[0m"
      echo -e "\033[32m ========================================================== \033[0m"
      exit 0
      ;;
    e)
      export_package=1
      ;;
    s)
      echo "build dependent package from source code."
      source_build=1
      ;;
    :)
      echo "Option -$OPTARG requires an argument."
      exit 1
      ;;
    *)
      echo "Invalid option: -$OPTARG"
      usage
      ;;
  esac
done

if [ ${arch} = "x64" ]; then
    build_arch=x64
    platform=default
elif [ ${arch} = "aarch64" ]; then
    build_arch=aarch64
    platform=aarch64
fi

do_build

exit 0
