module udp_infer

go 1.18

require (
	github.com/emirpasic/gods v1.18.1
	github.com/panjf2000/gnet v1.6.6
	github.com/pebbe/zmq4 v1.2.9
	github.com/sirupsen/logrus v1.9.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

require (
	github.com/panjf2000/ants/v2 v2.4.8 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	go.uber.org/atomic v1.9.0 // indirect
	go.uber.org/multierr v1.7.0 // indirect
	go.uber.org/zap v1.21.0 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
)
