import os
from conans import ConanFile, AutoToolsBuildEnvironment, tools
import contextlib
import os,subprocess
import semver

required_conan_version = ">=1.33.0"

def get_version():
    git = tools.Git()
    try:
        # If the last commit is not a tag, git.get_tag() returns None
        if git.get_tag() != None:
            return git.get_tag()
        else:
            return ("%s_%s" % (git.get_branch(), git.get_revision()[0:7])).replace(' ','').replace('(','').replace(')','')
    except:
        return ""

class UDPINFERConan(ConanFile):
    name = "udp-infer"
    branch = "master"
    generators = "make"
    settings = "os", "arch", "build_type"
    _autotools = None
    keep_imports = True
    no_import_to_bin = ["*xeno*"]
    exports_sources="*"

    @property
    def _cmake_install_base_path(self):
        return "cmake"
    def init(self):
        try:
            self._semver = semver.semver(get_version(),False)
        except:
            self._semver = semver.semver("0.0.0",False)
        self.version = self._semver.version

    def configure(self):
        # self.options["sinsegyerte"].shared = self.options.shared
        pass

    def _configure_autotools(self):
        if self._autotools:
            return self._autotools
        self._autotools = AutoToolsBuildEnvironment(self)
        return self._autotools

    @contextlib.contextmanager
    def _build_context(self):
        with tools.environment_append(tools.RunEnvironment(self).vars):
            yield
    def strip_pe(self, dir):
        if self.settings.build_type == "Debug":
            return
        for parent, dirnames, filenames in os.walk(dir):
            for filename in filenames:
                # CMD: strip -s libxxx.so
                abs_fp = os.path.join(parent, filename)
                with open(abs_fp, 'rb') as fp:
                    flag1 = fp.read(4)
                if flag1 == b'\x7fELF':
                    try:
                        subprocess.run([("%s -s %s" % (tools.get_env("STRIP"),os.path.join(parent, filename)))], check = True,shell= True)
                    except subprocess.CalledProcessError as e:
                        self.output.error("strip %s failed! %s" % (os.path.join(parent, filename),e))


    def source(self):
        self.run("git clone https://GIT_ACCESS_TOKEN:%s@gitlab.com/oppl2/SCORE.git && cd s-core && git checkout %s" %
                 (tools.get_env("GIT_ACCESS_TOKEN"), self.branch))

    def build(self):
        with tools.chdir(self.source_folder):
            autotools = self._configure_autotools()
            env_build_vars = autotools.vars
            env_build_vars['BIN'] = '%s/bin' % (self.build_folder)
            autotools.make(target="clean")
            autotools.make(vars=env_build_vars)

    def package(self):
        deb_package_name = self.name + self.version
        self.copy("*", src="bin", dst=os.path.join(deb_package_name,"usr/local/bin"))
        self.copy("*.so*", src="lib", dst=os.path.join(deb_package_name,"usr/local/lib"),symlinks=True)
        self.strip_pe(os.path.join(self.package_folder,deb_package_name))
        self.copy("*control*", src="config", dst=os.path.join(deb_package_name,"DEBIAN"))
        with tools.chdir(os.path.join(self.package_folder)):
            self.run("dpkg-deb --build %s" % deb_package_name)

    def deploy(self):
        self.copy("*", symlinks=True)




