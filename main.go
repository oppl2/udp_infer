// Package main is just an entry point.
package main

import (
	"fmt"

	"udp_infer/server"
)

func main() {
	// go frameUtils.Worker()
	if err := server.ServerRun("conf.yaml"); err != nil {
		fmt.Printf("Server Run failed! err=[%v]", err)
	}
}
