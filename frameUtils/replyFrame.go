// Package frameUtils 数据帧处理相关模块
package frameUtils

import (
	"bytes"
	"encoding/binary"
)

type ReplyFrame struct {
	Head       uint32
	Length     uint32
	Code       uint32
	CodeStatus uint32
	CodeCount  uint32
	Body       [1000]byte
	Checksum   uint32
}

func NewRepWithRes(res *ResponsesFrame) (rep *ReplyFrame) {
	rep = new(ReplyFrame)
	rep.Head = res.Head
	rep.Length = res.Length
	rep.Code = res.Code
	rep.CodeCount = res.CodeCount
	rep.Body = res.Body
	rep.Checksum = res.Checksum
	return
}

func NewRep() (rep *ReplyFrame) {
	rep = new(ReplyFrame)
	return
}

func (rep *ReplyFrame) GetFrame() ([]byte, error) {
	return rep.toBytes()
}

func (rep *ReplyFrame) toBytes() ([]byte, error) {
	buf := &bytes.Buffer{}
	err := binary.Write(buf, binary.LittleEndian, *rep)
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
