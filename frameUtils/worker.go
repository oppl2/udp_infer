// Package frameUtils 数据帧处理相关模块
package frameUtils

import (
	"fmt"

	"github.com/panjf2000/gnet"
	"github.com/pebbe/zmq4"
	"github.com/sirupsen/logrus"
)

func init() {
	err := zeromqClient()
	if err != nil {
		logrus.Panicln("init zeromq failed", err)
	}
}

var count int32

func Worker(frame []byte, c gnet.Conn) {
	count++
	logrus.WithFields(
		logrus.Fields{
			"frameLen": len(frame),
			"count":    count,
		},
	).Debugln("数据帧调试打印")

	if len(frame) <= 0 {
		logrus.Warningln("Invalid frame, ignore.")
		return
	}
	// frame 转 ResponsesFrame
	res, err := NewRes(frame)
	if err != nil {
		logrus.Errorln(err)
		return
	}
	// 数据帧检查HEAD
	if err = res.CheckResponses(); err != nil {
		logrus.Errorln(err)
		return
	}
	// 运行指令
	if err = res.RunCode(c); err != nil {
		logrus.Errorln(err)
		return
	}
}

func sendImageTest(img []byte) {
	if count%1000 == 0 {
		_, err := zmq.SendBytes(img, zmq4.DONTWAIT)
		if err != nil {
			fmt.Println("zeromq: ", err)
		}
	}
}
