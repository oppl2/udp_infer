// Package frameUtils 数据帧处理相关模块
package frameUtils

import (
	"bytes"
	"encoding/binary"
	"fmt"

	"github.com/panjf2000/gnet"
)

type ResponsesFrame struct {
	Head      uint32
	Length    uint32
	Code      uint32
	CodeCount uint32
	Body      [1000]byte
	Checksum  uint32
}

func NewRes(frame []byte) (res *ResponsesFrame, err error) {
	res = new(ResponsesFrame)
	err = binary.Read(bytes.NewReader(frame), binary.LittleEndian, res)
	return
}

func (rs *ResponsesFrame) CheckResponses() error {
	if rs.Head != validHEAD {
		return fmt.Errorf("Invalid HEAD of frame,HEAD=%X", rs.Head)
	}
	// 是否检验 checksum
	// if md5.Sum(frame) != rs.r.Checksum {
	// 	return fmt.Errorf("Checksum mismatch calc checksum=%d,recv checksum=%d", md5.Sum(frame),rs.r.Checksum)
	// }
	return nil
}

// RunCode 运行指令字，可启用协程
func (rs *ResponsesFrame) RunCode(c gnet.Conn) error {
	switch rs.Code {
	case INVALID_Code:
		return fmt.Errorf("InstructionCode invalid Code")
	case AI_STATUS_CHECK:
		fmt.Sprintln("Nothing", "to", "do", "!")
		// 模拟拼接ack frame
		ackframe, _ := NewRepWithRes(rs).toBytes()
		return rs.ack(ackframe, c)
	case PREPARE_UPLOAD_IMAGE:
		go fmt.Println("to prepare image")
	case UPLOAD_IMAGE:
		go fmt.Println("to upload image")
	case RECEIVE_IMAGE_STATUS:
		go fmt.Println("to recv image status")
	case ENABLE_RECOGNITION:
		fakeIMG := []byte("hello image hello image hello image hello image hello image")
		sendImageTest(fakeIMG)
	case RECOGNITION_STATUS:
		go fmt.Println("识别结果查询")
	case PREPARE_BINDING_DATA:
		fmt.Println("数据装订准备")
	case BINDING_DATA:
		fmt.Println("数据装订")
	case BINDING_DATA_STATUS:
		fmt.Println("数据装订状态")
	case SLEEP_AI:
		fmt.Println("休眠")
	case WAKE_AI:
		fmt.Println("唤醒")
	default:
		return fmt.Errorf("Unknow InstructionCode Code %X", rs.Code)
	}
	return nil
}

func (rs *ResponsesFrame) ack(ackFrame []byte, c gnet.Conn) error {
	// 仅作为结果显示，release 请删除
	prefix := []byte(fmt.Sprintln("ack num", count))
	prefix = append(prefix, ackFrame...)
	return c.SendTo(prefix)
}
