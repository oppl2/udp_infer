// Package frameUtils 数据帧处理相关模块
package frameUtils

// InstructionCode 指令字集合
const (
	// INVALID_Code Invalid instruction code
	INVALID_Code uint32 = 0
	// AI_STATUS_CHECK 查询AI加速模块当前工作状态
	AI_STATUS_CHECK = 0x4501
	// PREPARE_UPLOAD_IMAGE 图像上传准备
	PREPARE_UPLOAD_IMAGE = 0x4502
	// UPLOAD_IMAGE 图像上传
	UPLOAD_IMAGE = 0x4503
	// RECEIVE_IMAGE_STATUS  图像接受查询
	RECEIVE_IMAGE_STATUS = 0x4504
	// ENABLE_RECOGNITION  启动识别
	ENABLE_RECOGNITION = 0x4505
	// RECOGNITION_STATUS  识别结果查询
	RECOGNITION_STATUS = 0x4506
	// PREPARE_BINDING_DATA  数据装订准备
	PREPARE_BINDING_DATA = 0x4507
	// BINDING_DATA  数据装订
	BINDING_DATA = 0x4508
	// BINDING_DATA_STATUS  数据装订状态
	BINDING_DATA_STATUS = 0x4509
	// SLEEP_AI 休眠
	SLEEP_AI = 0x450A
	// WAKE_AI 唤醒
	WAKE_AI = 0x450B
)

// CodeStatus 指令字执行状态
const (
	// INVALID_Code_Status Invalid instruction code
	INVALID_Code_Status = 0
	// RECV_OK_AND_PASS 收到指令，校验正确
	RECV_OK_AND_PASS = 0xAA00
	// RECV_OK_AND_FAILED 收到指令，校验失败
	RECV_OK_AND_FAILED = 0xAA44
	// CODE_RUNNING  执行中
	CODE_RUNNING = 0xBB11
	// CODE_RUN_SUCCESS  执行成功
	CODE_RUN_SUCCESS = 0xBB33
	// CODE_RUN_FAILED  执行失败
	CODE_RUN_FAILED = 0xBB44
)

const (
	validHEAD = 0xAA55AA55
)
