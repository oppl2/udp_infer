// Package frameUtils 数据帧处理相关模块
package frameUtils

import (
	"github.com/pebbe/zmq4"
)

const endpoint string = "ipc:///tmp/sinserver.ipc"

var zmq *zmq4.Socket

func zeromqClient() error {
	zctx, err := zmq4.NewContext()
	if err != nil {
		return err
	}
	_publisher, err := zctx.NewSocket(zmq4.PUSH)
	if err != nil {
		return err
	}
	err = _publisher.Connect(endpoint)
	if err != nil {
		return err
	}
	zmq = _publisher
	return nil
}
