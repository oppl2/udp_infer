// Package server High-speed recv server test
package server

import (
	"fmt"
	"testing"
)

var conf = FilePath("../conf.yaml")

func TestServerRun(t *testing.T) {
	if err := ServerRun(conf); err != nil {
		fmt.Printf("Server Run failed! err=[%v]", err)
	}
}
