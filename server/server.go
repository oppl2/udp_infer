// Package server High-speed recv server
package server

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/sirupsen/logrus"

	"udp_infer/frameUtils"

	"github.com/panjf2000/gnet"
	"github.com/panjf2000/gnet/pkg/pool/goroutine"
	"gopkg.in/yaml.v3"
)

// FilePath path for string
type FilePath string

type protocalData struct {
	Conf struct {
		Protocol string `yaml:"protocol"`
		Port     uint32
	}
}

type sinServer struct {
	*gnet.EventServer
	pool *goroutine.Pool
}

func init() {
	logrus.SetReportCaller(true)
	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetOutput(os.Stdout)
}

func readConf(filename FilePath) (*protocalData, error) {
	buf, err := ioutil.ReadFile(string(filename))
	if err != nil {
		return nil, err
	}
	c := &protocalData{}
	err = yaml.Unmarshal(buf, c)
	if err != nil {
		logrus.Fatalf("Not found server config file %s, err: %v", filename, err)
	}
	return c, nil
}

func (ss *sinServer) OnInitComplete(srv gnet.Server) (action gnet.Action) {
	logrus.Debugf("UDP server is listening on %s (multi-cores: %t, loops: %d)\n",
		srv.Addr.String(), srv.Multicore, srv.NumEventLoop)
	return
}

func (ss *sinServer) React(frame []byte, c gnet.Conn) (out []byte, action gnet.Action) {
	data := append([]byte{}, frame...)
	if err := ss.pool.Submit(func() {
		frameUtils.Worker(data, c)
	}); err != nil {
		logrus.Errorln(err)
	}
	return
}

func (ss *sinServer) OnClosed(c gnet.Conn, err error) (action gnet.Action) {
	logrus.Debugf("UDP server is closed\n")
	return
}

// ServerRun run gnet server
func ServerRun(confPath FilePath) error {
	conf, err := readConf(confPath)
	if err != nil {
		return err
	}
	multicore, reuseport := true, true
	ss := new(sinServer)
	ss.pool = goroutine.Default()
	defer ss.pool.Release()
	gnet.WithSocketRecvBuffer(1024 * 1024 * 50)
	logrus.Fatal(gnet.Serve(ss, fmt.Sprintf("%s://:%d", conf.Conf.Protocol, conf.Conf.Port), gnet.WithMulticore(multicore), gnet.WithReusePort(reuseport)))
	return nil
}
